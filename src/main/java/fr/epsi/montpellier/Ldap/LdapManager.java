package fr.epsi.montpellier.Ldap;

import org.apache.logging.log4j.Logger;

import javax.naming.NamingException;
import java.util.List;

public interface LdapManager {
    String getUsersLdapDirectory();

    void setUsersLdapDirectory(String usersLdapDirectory);

    public void setLogger(Logger logger);

    void close();

    List<UserLdap> listUsers(String classe);

    List<UserLdap> listUsers();

    List<UserLdap> listUsersOfGroups(String group);

    UserLdap getUserFromDN(String dn);

    UserLdap getUser(String login);

    void addUser(String login, String nom, String prenom, String motDePasse,
                 String email, String classe, String role) throws NamingException, LdapException;

    void addUser(UserLdap user) throws NamingException, LdapException;

    boolean updateUser(String login, UserLdap userToUpdate) throws NamingException;

    boolean updateUserPassword(String login, String password) throws NamingException;

    boolean deactivateUser(String login) throws NamingException;

    boolean activateUser(String login) throws NamingException;

    boolean deleteUser(String login) throws NamingException;

    /*public boolean setNAClassToUsers() throws NamingException {
            // IMPORTANT: Seuls les utilisateurs du groupe Etudiants sont concernés !

            return setClassNAForUsers(ATTRIBUTE_NAME_PREVCLASSE);
        }
        public boolean setUsersToNA() throws NamingException {

            // IMPORTANT: Seuls les utilisateurs du groupe Etudiants sont concernés !
            return setClassNAForUsers(ATTRIBUTE_NAME_CLASSE);
        }*/
    double setPreviousClassNAForAllUsers() throws NamingException;

    double setClassNAForAllUsers() throws NamingException;

    UserLdap authenticateUser(String username, String password);
}
